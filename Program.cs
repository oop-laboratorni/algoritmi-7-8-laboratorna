﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//class Node
//{
//    public string Name { get; }
//    public List<Node> Children { get; }
//    public Node(string name)
//    {
//        Name = name;
//        Children = new List<Node>();
//    }
//    public Node DodNasl(Node node, bool bidirect = true)
//    {
//        Children.Add(node);
//        if (bidirect)
//        {
//            node.Children.Add(this);
//        }
//        return this;
//    }
//    private static void Print(LinkedList<Node> path)
//    {
//        if (path.Count == 0)
//        {
//            Console.WriteLine(" Помилка!");
//        }
//        else
//        {
//            Console.WriteLine(string.Join(" -> ", path.Select(x => x.Name)));
//        }
//        Console.WriteLine();
//    }
//    static void Main(string[] args)
//    {
//        Console.InputEncoding = Encoding.Unicode;
//        Console.OutputEncoding = Encoding.Unicode;
//        Node n01 = new Node("Київ");
//        Node n02 = new Node("Житомир - 135 ");
//        Node n03 = new Node("Новоград-Волинський - 80 ");
//        Node n04 = new Node("Рівне - 100 ");
//        Node n05 = new Node("Луцьк - 68 ");
//        Node n06 = new Node("Бердичів - 38 ");
//        Node n07 = new Node("Вінниця - 73 ");
//        Node n08 = new Node("Хмельницький - 110 ");
//        Node n09 = new Node("Тернопіль - 104 ");
//        Node n10 = new Node("Шепетівка - 115 ");
//        Node n11 = new Node("Біла церква -  78 ");
//        Node n12 = new Node("Умань - 115 ");
//        Node n13 = new Node("Черкаси - 146 ");
//        Node n14 = new Node("Кременчуг - 105 ");
//        Node n15 = new Node("Полтава - 181 ");
//        Node n16 = new Node("Харків - 130 ");
//        Node n17 = new Node("Прилуки - 128 ");
//        Node n18 = new Node("Суми - 175 ");
//        Node n19 = new Node("Миргород - 109 ");
//        n01.DodNasl(n02).DodNasl(n11).DodNasl(n17);
//        n02.DodNasl(n03).DodNasl(n06).DodNasl(n10);
//        n03.DodNasl(n04);
//        n04.DodNasl(n05);
//        n06.DodNasl(n07);
//        n07.DodNasl(n08);
//        n08.DodNasl(n09);
//        n11.DodNasl(n12).DodNasl(n13).DodNasl(n15);
//        n13.DodNasl(n14);
//        n15.DodNasl(n16);
//        n17.DodNasl(n18).DodNasl(n19);
//        var search = new BreadthFirstSearch();
//        var path = search.BFS(n01, n05);
//        Print(path);
//        var path1 = search.BFS(n01, n09);
//        Print(path1);
//        var path2 = search.BFS(n01, n10);
//        Print(path2);
//        var path3 = search.BFS(n01, n12);
//        Print(path3);
//        var path4 = search.BFS(n01, n14);
//        Print(path4);
//        var path5 = search.BFS(n01, n16);
//        Print(path5);
//        var path6 = search.BFS(n01, n18);
//        Print(path6);
//        var path7 = search.BFS(n01, n19);
//        Print(path7);
//        var path8 = search.BFS(n01, n02);
//        Print(path8);
//        var path9 = search.BFS(n01, n03);
//        Print(path9);
//        var path10 = search.BFS(n01, n04);
//        Print(path10);
//        var path11 = search.BFS(n01, n06);
//        Print(path11);
//        var path12 = search.BFS(n01, n07);
//        Print(path12);
//        var path13 = search.BFS(n01, n08);
//        Print(path13);
//        var path14 = search.BFS(n01, n11);
//        Print(path14);
//        var path15 = search.BFS(n01, n13);
//        Print(path15);
//        var path16 = search.BFS(n01, n15);
//        Print(path16);
//        var path17 = search.BFS(n01, n17);
//        Print(path17);
//        Console.Read();
//    }

//}
//class BreadthFirstSearch
//{
//    private HashSet<Node> visited;
//    private LinkedList<Node> path_BFS;
//    private Node goal;
//    public LinkedList<Node> BFS(Node start, Node goal)
//    {
//        visited = new HashSet<Node>();
//        path_BFS = new LinkedList<Node>();
//        this.goal = goal;
//        BFS(start);
//        if (path_BFS.Count > 0)
//        {
//            path_BFS.AddLast(start);
//        }
//        return path_BFS;
//    }
//    private bool BFS(Node node)
//    {
//        if (node == goal)
//        {
//            return true;
//        }
//        visited.Add(node);
//        foreach (var child in node.Children.Where(x => !visited.Contains(x)))
//        {
//            if (BFS(child))
//            {
//                path_BFS.AddLast(child);
//                return true;
//            }
//        }
//        return false;
//    }
//}








//DFS
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
class Node
{
    public string Name { get; }
    public List<Node> Children { get; }
    public Node(string name)
    {
        Name = name;
        Children = new List<Node>();
    }
    public Node DodNasl(Node node, bool bidirect = true)
    {
        Children.Add(node);
        if (bidirect)
        {
            node.Children.Add(this);
        }
        return this;
    }
    private static void Print(LinkedList<Node> path)
    {
        if (path.Count == 0)
        {
            Console.WriteLine("Сталася помилка!");
        }
        else
        {
            Console.WriteLine(string.Join(" -> ", path.Select(x => x.Name)));
        }
        Console.WriteLine();
    }
    static void Main(string[] args)
    {
        Console.InputEncoding = Encoding.Unicode;
        Console.OutputEncoding = Encoding.Unicode;
        Node n01 = new Node("Київ");
        Node n02 = new Node(" 135 - Житомир");
        Node n03 = new Node(" 80 - Новоград-Волинський");
        Node n04 = new Node(" 100 - Рівне");
        Node n05 = new Node(" 68 - Луцьк");
        Node n06 = new Node(" 38 - Бердичів");
        Node n07 = new Node(" 73 - Вінниця");
        Node n08 = new Node(" 110 - Хмельницький");
        Node n09 = new Node(" 104 - Тернопіль");
        Node n10 = new Node(" 115 - Шепетівка");
        Node n11 = new Node(" 78 - Біла церква");
        Node n12 = new Node(" 115 - Умань");
        Node n13 = new Node(" 146 - Черкаси");
        Node n14 = new Node(" 105 - Кременчуг");
        Node n15 = new Node(" 181 - Полтава");
        Node n16 = new Node(" 130 - Харків");
        Node n17 = new Node(" 128 - Прилуки");
        Node n18 = new Node(" 175 - Суми");
        Node n19 = new Node(" 109 - Миргород");
        n01.DodNasl(n02).DodNasl(n11).DodNasl(n17);
        n02.DodNasl(n03).DodNasl(n06).DodNasl(n10);
        n03.DodNasl(n04);
        n04.DodNasl(n05);
        n06.DodNasl(n07);
        n07.DodNasl(n08);
        n08.DodNasl(n09);
        n11.DodNasl(n12).DodNasl(n13).DodNasl(n15);
        n13.DodNasl(n14);
        n15.DodNasl(n16);
        n17.DodNasl(n18).DodNasl(n19);
        var search = new DepthFirstSearch();
        var path = search.DFS(n01, n05);
        Print(path);
        var path1 = search.DFS(n01, n09);
        Print(path1);
        var path2 = search.DFS(n01, n10);
        Print(path2);
        var path3 = search.DFS(n01, n12);
        Print(path3);
        var path4 = search.DFS(n01, n14);
        Print(path4);
        var path5 = search.DFS(n01, n16);
        Print(path5);
        var path6 = search.DFS(n01, n18);
        Print(path6);
        var path7 = search.DFS(n01, n19);
        Print(path7);
        var path8 = search.DFS(n01, n02);
        Print(path8);
        var path9 = search.DFS(n01, n03);
        Print(path9);
        var path10 = search.DFS(n01, n04);
        Print(path10);
        var path11 = search.DFS(n01, n06);
        Print(path11);
        var path12 = search.DFS(n01, n07);
        Print(path12);
        var path13 = search.DFS(n01, n08);
        Print(path13);
        var path14 = search.DFS(n01, n11);
        Print(path14);
        var path15 = search.DFS(n01, n13);
        Print(path15);
        var path16 = search.DFS(n01, n15);
        Print(path16);
        var path17 = search.DFS(n01, n17);
        Print(path17);
        Console.Read();
    }

}
class DepthFirstSearch
{
    private HashSet<Node> visit;
    private LinkedList<Node> ZStartToFin;
    private Node goal;
    public LinkedList<Node> DFS(Node start, Node goal)
    {
        visit = new HashSet<Node>();
        ZStartToFin = new LinkedList<Node>();
        this.goal = goal;
        DFS(start);
        if (ZStartToFin.Count > 0)
        {
            ZStartToFin.AddFirst(start);
        }
        return ZStartToFin;
    }
    private bool DFS(Node node)
    {
        if (node == goal)
        {
            return true;
        }
        visit.Add(node);
        foreach (var child in node.Children.Where(x => !visit.Contains(x)))
        {
            if (DFS(child))
            {
                ZStartToFin.AddFirst(child);
                return true;
            }
        }
        return false;
    }
}














